#ifndef MYFILESYSTEMMODEL_H
#define MYFILESYSTEMMODEL_H

#include <QtWidgets/QtWidgets>


class MyFileSystemModel : public QFileSystemModel
{
    typedef QFileSystemModel super;
public:
    virtual QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const
    {
        if (role == Qt::EditRole)
        {
            return QVariant(fileInfo(index).baseName());
        }
        else return QFileSystemModel::data(index, role);
    }

    virtual bool setData ( const QModelIndex & idx, const QVariant & value, int role = Qt::EditRole )
    {
        if (role == Qt::EditRole)
        {
            return QFileSystemModel::setData( idx, QVariant(value.toString() + "." + fileInfo(idx).completeSuffix()), role);
        }
        else return QFileSystemModel::setData(idx, value, role);
    }
protected:
    virtual Qt::ItemFlags flags(const QModelIndex & index) const override
    {
        // Retreive initial flags from QFileSystemModel's implementation
        Qt::ItemFlags flags = super::flags(index);

        // Retain all flags except for the Editable flag
        flags &= ~Qt::ItemIsEditable;

        // Enable editing for all file types except directories
        if(!(this->isDir(index)))
            flags |= Qt::ItemIsEditable;

        return flags;
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const {
        if ((role == Qt::DisplayRole)) {
            return QVariant(tr("Nom"));
        } else {
            return QFileSystemModel::headerData(section,orientation,role);
        }
    }
};

#endif // MYFILESYSTEMMODEL_H
