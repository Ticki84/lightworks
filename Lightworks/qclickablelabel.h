#ifndef QCLICKABLELABEL_H
#define QCLICKABLELABEL_H

#include <QtWidgets/QtWidgets>


class QClickableLabel : public QLabel
{
Q_OBJECT
public:
    explicit QClickableLabel( const QString& text="", QWidget* parent=0, const int& number=0, const int& id=0 );
    int number();
    int id();
    ~QClickableLabel();
private:
    int m_id;
    int m_number;
signals:
    void clicked();
protected:
    void mousePressEvent(QMouseEvent* event);
};

#endif // QCLICKABLELABEL_H
