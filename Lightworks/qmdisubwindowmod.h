#ifndef QMDISUBWINDOWMOD_H
#define QMDISUBWINDOWMOD_H

#include <QtWidgets/QtWidgets>
#include "qclickablelabel.h"
class FenetreEdition;


class QMdiSubWindowMod : public QMdiSubWindow
{
Q_OBJECT
public:
    explicit QMdiSubWindowMod(const int page, const int pageEffets, const int num, int id, QWidget * parent = 0, Qt::WindowFlags flags = 0);
    void setID(int id);
    int const ID();
    int const page();
    int const pageEffets();
    int const num();
    void setPrevPos(int pos);
    int prevPos();
    void getPtrFenetreEdition(FenetreEdition* fen);
    ~QMdiSubWindowMod();
private:
    const int m_page;
    const int m_pageEffets;
    const int m_num;
    int m_ID;
    int m_prevPos;
    FenetreEdition *m_ptrFenetreEdition;
    void closeEvent(QCloseEvent *event);
};

#endif // QMDISUBWINDOWMOD_H
