#ifndef QCLICKABLEPAINTER_H
#define QCLICKABLEPAINTER_H

#include <QtWidgets/QtWidgets>

class QClickablePainter : public QPainter
{
public:
    explicit QClickablePainter(QPaintDevice * device, const int& color);
    int color();
    ~QClickablePainter();
private:
    int m_color;
signals:
    void clicked();
protected:
    void mousePressEvent(QMouseEvent* event);
};

#endif // QCLICKABLEPAINTER_H
