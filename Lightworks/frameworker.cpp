#include "frameworker.h"

using namespace std;

FrameWorker::FrameWorker()
{

}

FrameWorker::~FrameWorker()
{
    //vector<int>().swap(m_listeFrame);
    m_listeFrame.clear();
}

void FrameWorker::h_sauverFrame(int page, int pageEffets, int num, int frame, int bouton, int couleur)
{
    if((int)m_listeFrame.size() != 0)
    {
        bool estExistant(false);
        for (int i=0; i<(int)m_listeFrame.size(); i++)
        {
            ContainerFrame *tempFrame = m_listeFrame[i];
            if (tempFrame->existe(page, pageEffets, num, frame))
            {
                estExistant = true;
                tempFrame->ajouterSupprBouton(bouton, couleur);
                break;
            }
        }
        if (!estExistant)
        {
            emit erreur(QString("La frame n'existe pas."));
        }
    }
    else
    {
        emit erreur(QString("Le conteneur de frames est vide."));
    }
}

void FrameWorker::h_chargerFrame(int page, int pageEffets, int num, int frame)
{
    bool estExistant(false);
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num, frame))
        {
            estExistant = true;
            vector<int> liste = tempFrame->listeBouton();
            emit updateEdition(liste, tempFrame->duree());
            break;
        }
    }
    if (!estExistant)
    {
        emit erreur(QString("La frame (%1, %2, %3, %4) est introuvable.").arg(page).arg(pageEffets).arg(num).arg(frame));
    }
}

void FrameWorker::h_ajouterFramePressed(int page, int pageEffets, int num, int frameActuelle)
{
    int fMax(1);
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if(tempFrame->existe(page, pageEffets, num))
        {
            fMax++;
            if(tempFrame->frame()>frameActuelle)
            {
                tempFrame->setFrame(tempFrame->frame()+1);
            }
        }
    }
    ContainerFrame *frameSauv = new ContainerFrame(page, pageEffets, num, frameActuelle+1, 50);
    m_listeFrame.push_back(frameSauv);

    emit update_ajouterFrame(fMax);
}

void FrameWorker::h_supprimerFramePressed(int page, int pageEffets, int num, int frameActuelle)
{
    int i(0);
    for (i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num, frameActuelle))
        {
            m_listeFrame.erase(m_listeFrame.begin()+i);
            break;
        }
    }
    int fMax(0);
    for (int j=0; j<(int)m_listeFrame.size(); j++)
    {
        ContainerFrame *tempFrame = m_listeFrame[j];
        if(tempFrame->existe(page, pageEffets, num))
        {
            fMax++;
            if(tempFrame->frame()>i)
            {
                tempFrame->setFrame(tempFrame->frame()-1);
            }
        }
    }

    emit update_supprimerFrame(fMax);
}

void FrameWorker::h_dupliquerFramePressed(int page, int pageEffets, int num, int frameActuelle, int dureeFrame)
{
    ContainerFrame *frameSauv = new ContainerFrame(page, pageEffets, num, frameActuelle+1, dureeFrame);
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num, frameActuelle))
        {
            frameSauv->setListeBouton(tempFrame->listeBouton());
            break;
        }
    }
    int fMax(1);
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if(tempFrame->existe(page, pageEffets, num))
        {
            fMax++;
            if(tempFrame->frame()>frameActuelle)
            {
                tempFrame->setFrame(tempFrame->frame()+1);
            }
        }
    }
    m_listeFrame.push_back(frameSauv);

    emit update_dupliquerFrame(fMax);
}

void FrameWorker::h_copierFramePressed(int page, int pageEffets, int num, int frameActuelle, int dureeFrame)
{
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num, frameActuelle))
        {
            m_clipboardBtn = tempFrame->listeBouton();
            m_clipboardDuree = dureeFrame;
            emit update_copierFrame();
        }
    }
}

void FrameWorker::h_collerFramePressed(int page, int pageEffets, int num, int frameActuelle)
{
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num, frameActuelle))
        {
            tempFrame->setListeBouton(m_clipboardBtn);
            tempFrame->setDuree(m_clipboardDuree);
            emit update_collerFrame();
        }
    }
}

void FrameWorker::h_dureeFrameValueChanged(int page, int pageEffets, int num, int frameActuelle, int dureeFrame)
{
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num, frameActuelle))
        {
            tempFrame->setDuree(dureeFrame);
        }
    }
}

void FrameWorker::h_update_supprimerPage(int page, int pageEffets, int num)
{
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num))
        {
            m_listeFrame.erase(m_listeFrame.begin()+i);
        }
    }
}

void FrameWorker::h_chargerPage(int page, int pageEffets)
{
    vector<bool> liste(80, false);
    for (int i = 0; i < 80; i++)
    {
        for (int j=0; j<(int)m_listeFrame.size(); j++)
        {
            ContainerFrame *tempFrame = m_listeFrame[j];
            if (tempFrame->existe(page, pageEffets, i))
            {
                liste[i] = true;
                break;
            }
        }
    }
    emit updateSelection(liste);
}

void FrameWorker::h_copierPage(int page, int pageEffets, int num)
{
    m_pageClipboard.clear();
    m_dureeClipboard.clear();
    int j(0);
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num))
        {
            j++;
        }
    }
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        for (int k=0; k<j; k++)
        {
            ContainerFrame *tempFrame = m_listeFrame[i];
            if (tempFrame->existe(page, pageEffets, num, k+1))
            {
                m_pageClipboard.push_back(tempFrame->listeBouton());
                m_dureeClipboard.push_back(tempFrame->duree());
            }
        }
    }
}

void FrameWorker::h_collerPage(int page, int pageEffets, int num)
{
    emit supprimerPageB(page, pageEffets, num);
}

void FrameWorker::h_update_supprimerPageB(int page, int pageEffets, int num)
{
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num))
        {
            m_listeFrame.erase(m_listeFrame.begin()+i);
        }
    }
    for(int i=0; i<(int)m_pageClipboard.size(); i++)
    {
        ContainerFrame *frameSauv = new ContainerFrame(page, pageEffets, num, i+1, m_dureeClipboard[i]);
        frameSauv->setListeBouton(m_pageClipboard[i]);
        m_listeFrame.push_back(frameSauv);
    }
    emit h_chargerPage(page, pageEffets);
}

void FrameWorker::h_rafraichirSelection(int page, int pageEffets, int num)
{
    int fMax(0);
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if(tempFrame->existe(page, pageEffets, num))
        {
            fMax++;
        }
    }
    if(fMax == 0)
    {
        ContainerFrame *frameSauv = new ContainerFrame(page, pageEffets, num, 1, 50);
        m_listeFrame.push_back(frameSauv);
        fMax++;
    }
    emit update_rafraichirSelection(fMax);
}

void FrameWorker::h_supprimerPatDir(QString path, bool isDir)
{
    if (!isDir)
    {
        if (QFile::remove(path) != 1)
        {
            emit erreur(QString("Impossible de supprimer le fichier"));
        }
    }
    else
    {
        if (removeDir(path) != 1)
        {
            emit erreur(QString("Impossible de supprimer le dossier"));
        }
    }
}

bool FrameWorker::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }

    return result;
}

void FrameWorker::h_savePattern(int page, int pageEffets, int num, QString path)
{
    bool endLoop = false;
    int base = 1;
    do {
        QFile file(QString("%1/Effet %2.lwe").arg(path).arg(base));
        if (file.exists())
        {
            base++;
        }
        else
        {
            endLoop = true;
            if (file.open(QIODevice::WriteOnly)) {
                QTextStream out(&file);
                //out.setVersion(QTextStream::Qt_5_4);
                int j(0);
                for (int i=0; i<(int)m_listeFrame.size(); i++)
                {
                    ContainerFrame *tempFrame = m_listeFrame[i];
                    if (tempFrame->existe(page, pageEffets, num))
                    {
                        j++;
                    }
                }
                if (j>0)
                {
                    for (int i=0; i<(int)m_listeFrame.size(); i++)
                    {
                        for (int k=0; k<j; k++)
                        {
                            ContainerFrame *tempFrame = m_listeFrame[i];
                            if (tempFrame->existe(page, pageEffets, num, k+1))
                            {
                                vector< int > listeBtn = tempFrame->listeBouton();
                                QString line = QString("%1,%2(").arg(k+1).arg(tempFrame->duree());
                                for (int l=0; l<79; l++)
                                {
                                    line += QString("%1,").arg(listeBtn[l]);
                                }
                                line += QString("%1)").arg(listeBtn[79]);
                                if (k+1 != j)
                                {
                                    line += QString("\n");
                                }
                                //out.writeRawData(line.toUtf8(), line.length());
                                out << line;
                                break;
                            }
                        }
                    }
                    file.close();
                }
                else
                {
                    file.close();
                    file.remove();
                    emit erreur("Vous ne pouvez pas enregistrer un prédéfini vide.");
                }
            }
            else
            {
                emit erreur("Impossible d'écrire le fichier.");
            }
        }
    } while (endLoop == false);
}

void FrameWorker::h_loadPattern(int page, int pageEffets, int num, QString pattern)
{
    QFile file(pattern);
    if (file.exists())
    {
        if (file.open(QIODevice::ReadOnly)) {
            m_conteneur.clear();
            QTextStream in(&file);
            while (!in.atEnd())
            {
                m_conteneur.push_back(in.readLine());
            }
            file.close();
            emit supprimerPageC(page, pageEffets, num);
        }
        else
        {
            emit erreur("Impossible d'ouvrir le fichier.");
        }
    }
}

void FrameWorker::h_update_supprimerPageC(int page, int pageEffets, int num)
{
    for (int i=0; i<(int)m_listeFrame.size(); i++)
    {
        ContainerFrame *tempFrame = m_listeFrame[i];
        if (tempFrame->existe(page, pageEffets, num))
        {
            m_listeFrame.erase(m_listeFrame.begin()+i);
        }
    }
    for(int i=0; i<(int)m_conteneur.size(); i++)
    {
        QString ligne = m_conteneur[i];
        int frame = ligne.section(',', 0, 0).toInt();
        int duree = ligne.section(',', 1, 1).section('(', 0, 0).toInt();
        ContainerFrame *frameSauv = new ContainerFrame(page, pageEffets, num, frame, duree);
        QString listeBtn = ligne.section('(', 1, 1).section(')', 0, 0);
        vector < int > listeBouton;
        for (int j=0; j<80; j++)
        {
            listeBouton.push_back(listeBtn.section(',', j, j).toInt());
        }
        frameSauv->setListeBouton(listeBouton);
        m_listeFrame.push_back(frameSauv);
    }
    emit h_chargerPage(page, pageEffets);
}

void FrameWorker::h_enregistrer(QString nom)
{
    QFile file(nom);
    if (file.open(QIODevice::WriteOnly)) {
        QTextStream out(&file);
        out << "inlets=2;outlets=3;var note=0,velocity=0,outNote=[],outColor=[],noteLength=[],page=1,decay=[],isRunning=[],currentNoteFrame=[],currentNoteLoop=[],noteBuffer=[],k=[],kBuffer=[],sendNote=new Task(fSendNote,this),indexOf=function(a){indexOf=\"function\"===typeof Array.prototype.indexOf?Array.prototype.indexOf:function(a){for(var b=-1,c=-1,b=0;b<this.length;b++)if(this[b]===a){c=b;break}return c};return indexOf.call(this,a)};" << endl;
        out << "function msg_int(a){if(0==inlet&&a!=note||1==inlet&&a!=velocity)if(0==inlet&&(note=a),1==inlet&&(velocity=a),0!=velocity&&0!=note){post(\"in\",\"-\",\"note:\",note,\"velocity:\",velocity,\"\\n\");outNote=[];outColor=[];noteLength=[];decay=[];switch(page){";
        emit initProgress(8*(int)m_listeFrame.size()+8*(int)m_listeFrame.size()*80);
        for (int page=1; page<9; page++)
        {
            for (int i=0; i<(int)m_listeFrame.size(); i++)
            {
                ContainerFrame *tempFrame = m_listeFrame[i];
                if (tempFrame->existe(page))
                {
                    out << "case " << page << ":switch(note){";
                }
                emit showProgress(page*i);
                break;
            }
            for (int i=0; i<(int)m_listeFrame.size(); i++)
            {
                for (int num=0; num<80; num++)
                {
                    ContainerFrame *tempFrame = m_listeFrame[i];
                    if (tempFrame->existe(page, -1, num))
                    {
                        out << "case " << num << ":switch(note){";
                    }
                }
                emit showProgress(page*(int)m_listeFrame.size()+page*i*80);
            }
        }
        emit closeProgress();
        file.close();
    }
    else
    {
        emit erreur("Impossible d'écrire le fichier.");
    }
}

bool FrameWorker::trier(const ContainerFrame& firstFrame, const ContainerFrame& secondFrame)
{
    /*return firstFrame.page()<secondFrame.page() ||
           (firstFrame.page()==secondFrame.page() && firstFrame.pageEffets()<secondFrame.pageEffets()) ||
           (firstFrame.page()==secondFrame.page() && firstFrame.pageEffets()==secondFrame.pageEffets() && firstFrame.frame()<secondFrame.frame());*/
    return std::forward_as_tuple(firstFrame.page(), firstFrame.pageEffets(), firstFrame.frame())
         < std::forward_as_tuple(secondFrame.page(), secondFrame.pageEffets(), secondFrame.frame());
}

int FrameWorker::noteToMidi(int note)
{
    switch(note)
    {
    case 0:
        return 64;
        break;
    case 1:
        return 65;
        break;
    case 2:
        return 66;
        break;
    case 3:
        return 67;
        break;
    case 4:
        return 96;
        break;
    case 5:
        return 97;
        break;
    case 6:
        return 98;
        break;
    case 7:
        return 99;
        break;
    case 8:
        return 60;
        break;
    case 9:
        return 61;
        break;
    case 10:
        return 62;
        break;
    case 11:
        return 63;
        break;
    case 12:
        return 92;
        break;
    case 13:
        return 93;
        break;
    case 14:
        return 94;
        break;
    case 15:
        return 95;
        break;
    case 16:
        return 56;
        break;
    case 17:
        return 57;
        break;
    case 18:
        return 58;
        break;
    case 19:
        return 59;
        break;
    case 20:
        return 88;
        break;
    case 21:
        return 89;
        break;
    case 22:
        return 90;
        break;
    case 23:
        return 91;
        break;
    case 24:
        return 52;
        break;
    case 25:
        return 53;
        break;
    case 26:
        return 54;
        break;
    case 27:
        return 55;
        break;
    case 28:
        return 84;
        break;
    case 29:
        return 85;
        break;
    case 30:
        return 86;
        break;
    case 31:
        return 87;
        break;
    case 32:
        return 48;
        break;
    case 33:
        return 49;
        break;
    case 34:
        return 50;
        break;
    case 35:
        return 51;
        break;
    case 36:
        return 80;
        break;
    case 37:
        return 81;
        break;
    case 38:
        return 82;
        break;
    case 39:
        return 83;
        break;
    case 40:
        return 44;
        break;
    case 41:
        return 45;
        break;
    case 42:
        return 46;
        break;
    case 43:
        return 47;
        break;
    case 44:
        return 76;
        break;
    case 45:
        return 77;
        break;
    case 46:
        return 78;
        break;
    case 47:
        return 79;
        break;
    case 48:
        return 40;
        break;
    case 49:
        return 41;
        break;
    case 50:
        return 42;
        break;
    case 51:
        return 43;
        break;
    case 52:
        return 72;
        break;
    case 53:
        return 73;
        break;
    case 54:
        return 74;
        break;
    case 55:
        return 75;
        break;
    case 56:
        return 36;
        break;
    case 57:
        return 37;
        break;
    case 58:
        return 38;
        break;
    case 59:
        return 39;
        break;
    case 60:
        return 68;
        break;
    case 61:
        return 69;
        break;
    case 62:
        return 70;
        break;
    case 63:
        return 71;
        break;
    case 64:
        return 108;
        break;
    case 65:
        return 109;
        break;
    case 66:
        return 110;
        break;
    case 67:
        return 111;
        break;
    case 68:
        return 112;
        break;
    case 69:
        return 113;
        break;
    case 70:
        return 114;
        break;
    case 71:
        return 115;
        break;
    case 72:
        return 100;
        break;
    case 73:
        return 101;
        break;
    case 74:
        return 102;
        break;
    case 75:
        return 103;
        break;
    case 76:
        return 104;
        break;
    case 77:
        return 105;
        break;
    case 78:
        return 106;
        break;
    case 79:
        return 107;
        break;
    default:
        return 100;
        break;
    }
}
