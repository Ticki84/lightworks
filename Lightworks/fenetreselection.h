#ifndef FENETRESELECTION_H
#define FENETRESELECTION_H

#include <QMainWindow>
#include "qclickablelabel.h"
#include "myfilesystemmodel.h"
#include <QtWidgets/QtWidgets>
#include <vector>


class FenetreSelection : public QMainWindow
{
    Q_OBJECT

public:
    explicit FenetreSelection(QWidget *parent = 0);
    ~FenetreSelection();

public slots:
    void const onm_pageValueChanged();
    void const onm_pageEffetsValueChanged();
    void onModeleSelectionChanged(int modele);
    void h_updateSelection(const std::vector<bool> &liste);
    void onm_editerClicked();
    void onm_supprimerClicked();
    void onm_copierClicked();
    void onm_collerClicked();
    void onm_sauverPatternClicked();
    void onm_collerPatternClicked();
    void onButtonClicked();
    void onActionEnregistrer();
    void onActionEnregistrerSous();
    void onActionToClipboard();
    void onActionCharger();
    void onActionFromClipboard();
    void rafraichir();
    void onm_vueClicked();
    void onm_supprimerPatDirClicked();
    void h_erreur(QString message);
signals:
    void chargerPage(int page, int pageEffets);
    void ouvrirOnglet(int modele, int page, int pageEffets, int num);
    void supprimerPage(int page, int pageEffets, int num);
    void copierPage(int page, int pageEffets, int num);
    void collerPage(int page, int pageEffets, int num);
    void savePattern(int page, int pageEffets, int num, QString path);
    void loadPattern(int page, int pageEffets, int num, QString pattern);
    void supprimerPatDir(QString path, bool isDir);
    void enregistrer(QString chemin);
private:
    int m_modele;
    int m_mode;
    std::vector< QClickableLabel* > *m_listeBouton;
    QClickableLabel *m_bouton;
    QSpinBox *m_page;
    QSpinBox *m_pageEffets;
    QPushButton *m_editer;
    QPushButton *m_supprimer;
    QPushButton *m_copier;
    QPushButton *m_coller;
    QPushButton *m_importerPattern;
    QPushButton *m_exporterPattern;
    //FenetreEdition *m_ptrFenetreEdition;
    QString m_sauvegarde;
    QPushButton *m_sauverPattern;
    QPushButton *m_collerPattern;
    QPushButton *m_supprimerPatDir;
    QTreeView *m_vue;
    MyFileSystemModel *m_dockModele;
    QString m_currentPath;
    QDir m_lwDir;
};

#endif // FENETRESELECTION_H
