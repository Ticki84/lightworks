#include "fenetreselection.h"
#include "fenetreedition.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FenetreSelection w;
    FenetreEdition v;
    v.getPtrFenetreSelection(&w);
    v.showMaximized();
    w.show();

    return a.exec();
}
