#ifndef FRAMEWORKER_H
#define FRAMEWORKER_H

#include <QtWidgets/QtWidgets>
#include <vector>
#include "containerframe.h"
#include <tuple>


class FrameWorker : public QObject
{
    Q_OBJECT

public:
    FrameWorker();
    static bool removeDir(const QString &dirName);
    int noteToMidi(int note);
    bool trier(const ContainerFrame& firstFrame, const ContainerFrame& secondFrame);
    ~FrameWorker();

public slots:
    void h_sauverFrame(int page, int pageEffets, int num, int frame, int bouton, int couleur);
    void h_chargerFrame(int page, int pageEffets, int num, int frame);
    void h_ajouterFramePressed(int page, int pageEffets, int num, int frameActuelle);
    void h_supprimerFramePressed(int page, int pageEffets, int num, int frameActuelle);
    void h_dupliquerFramePressed(int page, int pageEffets, int num, int frameActuelle, int dureeFrame);
    void h_copierFramePressed(int page, int pageEffets, int num, int frameActuelle, int dureeFrame);
    void h_collerFramePressed(int page, int pageEffets, int num, int frameActuelle);
    void h_dureeFrameValueChanged(int page, int pageEffets, int num, int frameActuelle, int dureeFrame);
    void h_update_supprimerPage(int page, int pageEffets, int num);
    void h_update_supprimerPageB(int page, int pageEffets, int num);
    void h_update_supprimerPageC(int page, int pageEffets, int num);
    void h_copierPage(int page, int pageEffets, int num);
    void h_collerPage(int page, int pageEffets, int num);
    void h_chargerPage(int page, int pageEffets);
    void h_rafraichirSelection(int page, int pageEffets, int num);
    void h_supprimerPatDir(QString path, bool isDir);
    void h_savePattern(int page, int pageEffets, int num, QString path);
    void h_loadPattern(int page, int pageEffets, int num, QString pattern);
    void h_enregistrer(QString nom);
signals:
    void updateEdition(const std::vector<int> &liste, int duree);
    void updateSelection(const std::vector<bool> &liste);
    void erreur(const QString &eMessage);
    void update_ajouterFrame(int fMax);
    void update_supprimerFrame(int fMax);
    void update_dupliquerFrame(int fMax);
    void update_copierFrame();
    void update_collerFrame();
    void supprimerPageB(int page, int pageEffets, int num);
    void supprimerPageC(int page, int pageEffets, int num);
    void update_rafraichirSelection(int fMax);
    void update_savePattern(QString nom);
    void initProgress(int max);
    void showProgress(int valeur);
    void closeProgress();

private:
    std::vector< ContainerFrame* > m_listeFrame;
    std::vector< int > m_clipboardBtn;
    int m_clipboardDuree;
    std::vector< std::vector < int > > m_pageClipboard;
    std::vector< int > m_dureeClipboard;
    std::vector<QString> m_conteneur;
};

#endif // FRAMEWORKER_H
