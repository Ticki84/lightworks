#include "qclickablelabel.h"


QClickableLabel::QClickableLabel(const QString& text, QWidget* parent, const int& number, const int& id)
    : QLabel(parent), m_number(number), m_id(id)
{
    setText(text);
}

QClickableLabel::~QClickableLabel()
{
}

void QClickableLabel::mousePressEvent(QMouseEvent* event)
{
    emit clicked();
}

int QClickableLabel::number()
{
    return m_number;
}

int QClickableLabel::id()
{
    return m_id;
}
