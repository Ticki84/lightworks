#include "qclickablepainter.h"

QClickablePainter::QClickablePainter(QPaintDevice * device, const int& color)
    : QPainter(device)
{
    m_color = color;
}

QClickablePainter::~QClickablePainter()
{
}

void QClickablePainter::mousePressEvent(QMouseEvent* event)
{
    emit clicked();
}

int QClickablePainter::color()
{
    return m_color;
}
