#include "fenetreedition.h"


using namespace std;

FenetreEdition::FenetreEdition()
{
    m_zoneCentrale = new QMdiArea;
    QObject::connect(m_zoneCentrale, SIGNAL(subWindowActivated(QMdiSubWindow *)), SLOT(rafraichirSelection()));

    m_clipboardRempli = false;

    m_mainFWorker = new FrameWorker();
    qRegisterMetaType< vector<int> >("vector<int>");
    qRegisterMetaType< vector<bool> >("vector<bool>");

    QWidget *toolBarContainer1 = new QWidget;
    QWidget *toolBarContainer2 = new QWidget;
    QWidget *toolBarContainer3 = new QWidget;
    m_selectionActuelle = QString("Aucune");
    m_boutonActuel = new QLabel(QString(tr("Sélection actuelle:"))+QString(" %1").arg(m_selectionActuelle));
    m_frameActuelle = new QSpinBox;
    m_frameActuelle->setMinimum(1);
    m_frameActuelle->setMaximum(1);
    m_frameActuelle->setPrefix(tr("Frame "));
    m_frameActuelle->setValue(1);
    m_frameActuelle->setEnabled(false);
    m_dureeFrame = new QSpinBox;
    m_dureeFrame->setMinimum(50);
    m_dureeFrame->setMaximum(10000);
    m_dureeFrame->setPrefix(tr("Durée: "));
    m_dureeFrame->setSuffix("ms");
    m_dureeFrame->setValue(10);
    m_dureeFrame->setEnabled(false);
    qDebug() << QDir::currentPath();
    m_ajouterFrame = new QPushButton(tr("Ajouter une frame"));
    m_ajouterFrame->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/add.png"));
    m_ajouterFrame->setEnabled(false);
    m_supprimerFrame = new QPushButton(tr("Supprimer cette frame"));
    m_supprimerFrame->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/remove.png"));
    m_supprimerFrame->setEnabled(false);
    m_dupliquerFrame = new QPushButton(tr("Dupliquer cette frame"));
    m_dupliquerFrame->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/copy.png"));
    m_dupliquerFrame->setEnabled(false);
    m_copierFrame = new QPushButton(tr("Copier cette frame"));
    m_copierFrame->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/copy.png"));
    m_copierFrame->setEnabled(false);
    m_collerFrame = new QPushButton(tr("Coller"));
    m_collerFrame->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/paste.png"));
    m_collerFrame->setEnabled(false);

    QHBoxLayout *toolBarLayout1 = new QHBoxLayout;
    QHBoxLayout *toolBarLayout2 = new QHBoxLayout;
    QHBoxLayout *toolBarLayout3 = new QHBoxLayout;
    toolBarLayout1->addWidget(m_boutonActuel);
    toolBarLayout1->addWidget(m_frameActuelle);
    toolBarLayout1->addWidget(m_dureeFrame);
    toolBarLayout2->addWidget(m_ajouterFrame);
    toolBarLayout2->addWidget(m_supprimerFrame);
    toolBarLayout2->addWidget(m_dupliquerFrame);
    toolBarLayout3->addWidget(m_copierFrame);
    toolBarLayout3->addWidget(m_collerFrame);
    toolBarContainer1->setLayout(toolBarLayout1);
    toolBarContainer2->setLayout(toolBarLayout2);
    toolBarContainer3->setLayout(toolBarLayout3);
    QToolBar *mainToolBar = addToolBar("Toolbar Principale");
    mainToolBar->addWidget(toolBarContainer1);
    mainToolBar->addSeparator();
    mainToolBar->addWidget(toolBarContainer2);
    mainToolBar->addSeparator();
    mainToolBar->addWidget(toolBarContainer3);
    mainToolBar->setMovable(false);

    QWidget *colorWidget = new QWidget;
    QDockWidget *mainDock = new QDockWidget;
    addDockWidget(Qt::LeftDockWidgetArea, mainDock);
    mainDock->setWindowTitle(tr("Palette"));
    mainDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    mainDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    QGridLayout *colorLayout = new QGridLayout;

    QPixmap *pixmap = new QPixmap(60,35);
    pixmap->fill(QColor("transparent"));
    QPainter painter(pixmap);
    painter.setRenderHint(QPainter::Antialiasing);
    QPainterPath path;
    path.addRoundedRect(QRectF(9.5, 9.5, 50, 25), 2, 2);
    QPen pen(Qt::black, 1);
    painter.setPen(pen);
    m_selectionCouleur = 0;
    for (int i=0; i<16; i++)
    {
        m_boutonCouleur = new QClickableLabel("", colorWidget, i);
        QColor currentColor(255, 255, 255);
        switch (i)
        {
        case 0:
            currentColor = QColor(0, 190, 0);
            break;
        case 1:
            currentColor = QColor(0, 150, 0);
            break;
        case 2:
            currentColor = QColor(0, 110, 0);
            break;
        case 3:
            currentColor = QColor(120, 255, 80);
            break;
        case 4:
            currentColor = QColor(97, 204, 65);
            break;
        case 5:
            currentColor = QColor(73, 153, 48);
            break;
        case 6:
            currentColor = QColor(255, 255, 0);
            break;
        case 7:
            currentColor = QColor(224, 224, 0);
            break;
        case 8:
            currentColor = QColor(255, 100, 0);
            break;
        case 9:
            currentColor = QColor(235, 88, 0);
            break;
        case 10:
            currentColor = QColor(220, 80, 0);
            break;
        case 11:
            currentColor = QColor(195, 70, 0);
            break;
        case 12:
            currentColor = QColor(255, 0, 0);
            break;
        case 13:
            currentColor = QColor(224, 0, 0);
            break;
        case 14:
            currentColor = QColor(200, 0, 0);
            break;
        case 15:
            break;
        }
        painter.fillPath(path, currentColor);
        painter.drawPath(path);
        m_boutonCouleur->setPixmap(*pixmap);
        switch (i)
        {
        case 0:
            colorLayout->addWidget(m_boutonCouleur, 0, 0);
            break;
        case 1:
            colorLayout->addWidget(m_boutonCouleur, 0, 1);
            break;
        case 2:
            colorLayout->addWidget(m_boutonCouleur, 0, 2);
            break;
        case 3:
            colorLayout->addWidget(m_boutonCouleur, 1, 0);
            break;
        case 4:
            colorLayout->addWidget(m_boutonCouleur, 1, 1);
            break;
        case 5:
            colorLayout->addWidget(m_boutonCouleur, 1, 2);
            break;
        case 6:
            colorLayout->addWidget(m_boutonCouleur, 2, 0);
            break;
        case 7:
            colorLayout->addWidget(m_boutonCouleur, 2, 1);
            break;
        case 8:
            colorLayout->addWidget(m_boutonCouleur, 2, 2);
            break;
        case 9:
            colorLayout->addWidget(m_boutonCouleur, 3, 0);
            break;
        case 10:
            colorLayout->addWidget(m_boutonCouleur, 3, 1);
            break;
        case 11:
            colorLayout->addWidget(m_boutonCouleur, 3, 2);
            break;
        case 12:
            colorLayout->addWidget(m_boutonCouleur, 4, 0);
            break;
        case 13:
            colorLayout->addWidget(m_boutonCouleur, 4, 1);
            break;
        case 14:
            colorLayout->addWidget(m_boutonCouleur, 4, 2);
            break;
        case 15:
            colorLayout->addWidget(m_boutonCouleur, 5, 1);
            break;
        }
        connect(m_boutonCouleur, SIGNAL(clicked()), SLOT(onColorClicked()));
    }

    colorWidget->setLayout(colorLayout);
    mainDock->setWidget(colorWidget);

    setCentralWidget(m_zoneCentrale);

    this->setWindowTitle(QString("Lightworks - ")+QString(tr("Fenêtre d'édition")));

    QObject::connect(m_frameActuelle, SIGNAL(valueChanged(int)), this, SLOT(onm_frameActuelleValueChanged()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(chargerFrame(int,int,int,int)), m_mainFWorker, SLOT(h_chargerFrame(int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(updateEdition(std::vector<int>,int)), this, SLOT(h_updateEdition(std::vector<int>,int)), Qt::QueuedConnection);
    //
    QObject::connect(m_dureeFrame, SIGNAL(valueChanged(int)), this, SLOT(onm_dureeFrameValueChanged()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(dureeFrameValueChanged(int,int,int,int,int)), m_mainFWorker, SLOT(h_dureeFrameValueChanged(int,int,int,int,int)), Qt::QueuedConnection);
    //
    QObject::connect(m_ajouterFrame, SIGNAL(released()), this, SLOT(onm_ajouterFramePressed()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(ajouterFramePressed(int,int,int,int)), m_mainFWorker, SLOT(h_ajouterFramePressed(int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(update_ajouterFrame(int)), this, SLOT(h_update_ajouterFrame(int)), Qt::QueuedConnection);
    //
    QObject::connect(m_supprimerFrame, SIGNAL(released()), this, SLOT(onm_supprimerFramePressed()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(supprimerFramePressed(int,int,int,int)), m_mainFWorker, SLOT(h_supprimerFramePressed(int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(update_supprimerFrame(int)), this, SLOT(h_update_supprimerFrame(int)), Qt::QueuedConnection);
    //
    QObject::connect(m_dupliquerFrame, SIGNAL(released()), this, SLOT(onm_dupliquerFramePressed()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(dupliquerFramePressed(int,int,int,int,int)), m_mainFWorker, SLOT(h_dupliquerFramePressed(int,int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(update_dupliquerFrame(int)), this, SLOT(h_update_dupliquerFrame(int)), Qt::QueuedConnection);
    //
    QObject::connect(m_copierFrame, SIGNAL(released()), this, SLOT(onm_copierFramePressed()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(copierFramePressed(int,int,int,int,int)), m_mainFWorker, SLOT(h_copierFramePressed(int,int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(update_copierFrame()), this, SLOT(h_update_copierFrame()), Qt::QueuedConnection);
    //
    QObject::connect(m_collerFrame, SIGNAL(released()), this, SLOT(onm_collerFramePressed()), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(collerFramePressed(int,int,int,int)), m_mainFWorker, SLOT(h_collerFramePressed(int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(update_collerFrame()), this, SLOT(h_update_collerFrame()), Qt::QueuedConnection);
    //
    QObject::connect(this, SIGNAL(sauverFrame(int,int,int,int,int,int)), m_mainFWorker, SLOT(h_sauverFrame(int,int,int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(s_rafraichirSelection(int,int,int)), m_mainFWorker, SLOT(h_rafraichirSelection(int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(update_rafraichirSelection(int)), this, SLOT(h_update_rafraichirSelection(int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(erreur(QString)), this, SLOT(h_erreur(QString)), Qt::QueuedConnection);
}

FenetreEdition::~FenetreEdition()
{
}

void FenetreEdition::h_ouvrirOnglet(int modele, int page, int pageEffets, int num)
{
    bool existe(false);
    int gridNumX(0), gridNumY(0);
    if (num < 64)
    {
        gridNumX = 1+(num%8);
        gridNumY = 1+(num/8);
    }
    else if (num > 63 && num <72)
    {
        gridNumX = 0;
        gridNumY = num-63;
    }
    else if (num > 71 && num <80)
    {
        gridNumX = num-71;
        gridNumY = 9;
    }
    QString title = QString("Bouton %1x%2 | Page %3 | Page d'effets %4").arg(gridNumX).arg(gridNumY).arg(page).arg(pageEffets);
    QList<QMdiSubWindow *> listeOnglets = m_zoneCentrale->subWindowList(QMdiArea::CreationOrder);
    for(int i=0; i<(int)listeOnglets.size(); i++)
    {
        QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(listeOnglets[i]);
        if(tempWindow->page() == page && tempWindow->pageEffets() == pageEffets && tempWindow->num() == num && tempWindow->windowTitle() != "Closed Window")
        {
            existe = true;
            m_zoneCentrale->setActiveSubWindow(listeOnglets[i]);
            break;
        }
    }
    if (existe == false)
    {
        if (modele == 0)
        {
            QWidget *edition = new QWidget;
            QGridLayout *layout = new QGridLayout(edition);
            for (int i = 0; i<80; i++) {
                m_bouton = new QClickableLabel("", edition, i);
                if (i != 27 && i != 28 && i != 35 && i != 36 && i<64){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72.png"));
                    layout->addWidget(m_bouton, 1+(i/8), i%8);
                }
                else if (i == 27){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172.png"));
                    layout->addWidget(m_bouton, 1+(i/8), i%8);
                }
                else if (i == 28){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272.png"));
                    layout->addWidget(m_bouton, 1+(i/8), i%8);
                }
                else if (i == 35){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372.png"));
                    layout->addWidget(m_bouton, 1+(i/8), i%8);
                }
                else if (i == 36){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472.png"));
                    layout->addWidget(m_bouton, 1+(i/8), i%8);
                }
                else if (i>63 && i<72){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
                    layout->addWidget(m_bouton, 0, i-64);
                }
                else if (i>71 && i<80){
                    m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
                    layout->addWidget(m_bouton, i-71, 9);
                }
                connect(m_bouton, SIGNAL(clicked()), SLOT(onButtonClicked()));
                m_listeBouton.push_back(m_bouton);
            }
            QLabel *logo = new QLabel("");
            logo->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/logo.png"));
            layout->addWidget(logo, 0, 9);
            edition->setLayout(layout);

            QMdiSubWindowMod *onglet = new QMdiSubWindowMod(page, pageEffets, num, (((int)m_listeBouton.size())/80)-1, m_zoneCentrale);
            onglet->setWidget(edition);
            onglet->getPtrFenetreEdition(this);
            onglet->setWindowTitle(title);
            m_zoneCentrale->addSubWindow(onglet);
            onglet->show();
            onglet->setFixedSize(onglet->size());

            if (m_zoneCentrale->activeSubWindow() == 0 || m_zoneCentrale->activeSubWindow()->windowTitle() == QString("Closed Window"))
            {
                m_selectionActuelle = QString("Aucune");
                m_frameActuelle->setValue(1);
                m_frameActuelle->setMaximum(1);
                m_frameActuelle->setEnabled(false);
                m_dureeFrame->setValue(50);
                m_dureeFrame->setEnabled(false);
                m_ajouterFrame->setEnabled(false);
                m_dupliquerFrame->setEnabled(false);
                m_copierFrame->setEnabled(false);
                m_collerFrame->setEnabled(false);
                m_boutonActuel->setText(QString("Sélection actuelle: %1").arg(m_selectionActuelle));
            }
            else
            {
                QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(m_zoneCentrale->activeSubWindow());
                m_page = tempWindow->page();
                m_pageEffets = tempWindow->pageEffets();
                m_num = tempWindow->num();

                emit s_rafraichirSelection(m_page, m_pageEffets, m_num);
            }
        }
        else
        {
            emit h_erreur("Modèle non disponible");
        }
    }
}

void const FenetreEdition::onm_frameActuelleValueChanged()
{
    m_frameActuelle->findChild<QLineEdit*>()->deselect();
    if(m_frameActuelle->value()==1)
    {
        m_supprimerFrame->setEnabled(false);
    }
    else
    {
        m_supprimerFrame->setEnabled(true);
    }
    QMdiSubWindowMod *onglet = static_cast<QMdiSubWindowMod*>(m_zoneCentrale->activeSubWindow());
    onglet->setPrevPos(m_frameActuelle->value());

    emit chargerFrame(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::onColorClicked()
{
    QClickableLabel *boutonEmetteur = qobject_cast<QClickableLabel *>( this->sender() );
    m_selectionCouleur = boutonEmetteur->number();
}

void FenetreEdition::onButtonClicked()
{
    QClickableLabel *boutonEmetteur = qobject_cast<QClickableLabel *>( this->sender() );
    int idBtn = boutonEmetteur->number();
    if (m_selectionCouleur == 15)
    {
        if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72.png"));
        }
        else if (idBtn == 27){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172.png"));
        }
        else if (idBtn == 28){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272.png"));
        }
        else if (idBtn == 35){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372.png"));
        }
        else if (idBtn == 36){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472.png"));
        }
        else if (idBtn>63 && idBtn<80){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
        }
    }
    else
    {
        if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72c.png"));
        }
        else if (idBtn == 27){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172c.png"));
        }
        else if (idBtn == 28){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272c.png"));
        }
        else if (idBtn == 35){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372c.png"));
        }
        else if (idBtn == 36){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472c.png"));
        }
        else if (idBtn>63 && idBtn<80){
            boutonEmetteur->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72c.png"));
        }
    }
    QGraphicsColorizeEffect *couleur = new QGraphicsColorizeEffect(boutonEmetteur);
    QColor couleurActuelle(255, 255, 255);
    switch (m_selectionCouleur)
    {
    case 0:
        couleurActuelle = QColor(0, 190, 0);
        couleur->setStrength(1.0);
        break;
    case 1:
        couleurActuelle = QColor(0, 190, 0);
        couleur->setStrength(0.8);
        break;
    case 2:
        couleurActuelle = QColor(0, 190, 0);
        couleur->setStrength(0.6);
        break;
    case 3:
        couleurActuelle = QColor(120, 255, 80);
        couleur->setStrength(1.0);
        break;
    case 4:
        couleurActuelle = QColor(120, 255, 80);
        couleur->setStrength(0.8);
        break;
    case 5:
        couleurActuelle = QColor(120, 255, 80);
        couleur->setStrength(0.6);
        break;
    case 6:
        couleurActuelle = QColor(255, 255, 0);
        couleur->setStrength(1.0);
        break;
    case 7:
        couleurActuelle = QColor(255, 255, 0);
        couleur->setStrength(0.8);
        break;
    case 8:
        couleurActuelle = QColor(255, 100, 0);
        couleur->setStrength(1.0);
        break;
    case 9:
        couleurActuelle = QColor(255, 100, 0);
        couleur->setStrength(0.8);
        break;
    case 10:
        couleurActuelle = QColor(255, 100, 0);
        couleur->setStrength(0.6);
        break;
    case 11:
        couleurActuelle = QColor(255, 100, 0);
        couleur->setStrength(0.4);
        break;
    case 12:
        couleurActuelle = QColor(255, 0, 0);
        couleur->setStrength(1.0);
        break;
    case 13:
        couleurActuelle = QColor(255, 0, 0);
        couleur->setStrength(0.8);
        break;
    case 14:
        couleurActuelle = QColor(255, 0, 0);
        couleur->setStrength(0.6);
        break;
    case 15:
        couleurActuelle = QColor(255, 255, 255);
        couleur->setStrength(0.0);
        break;
    }
    couleur->setColor(couleurActuelle);
    boutonEmetteur->setGraphicsEffect(couleur);

    emit sauverFrame(m_page, m_pageEffets, m_num, m_frameActuelle->value(), boutonEmetteur->number(), m_selectionCouleur);
}

void FenetreEdition::rafraichirSelection()
{
    if (m_zoneCentrale->activeSubWindow() == 0 || m_zoneCentrale->activeSubWindow()->windowTitle() == QString("Closed Window"))
    {
        m_selectionActuelle = QString("Aucune");
        m_frameActuelle->setValue(1);
        m_frameActuelle->setMaximum(1);
        m_frameActuelle->setEnabled(false);
        m_dureeFrame->setValue(50);
        m_dureeFrame->setEnabled(false);
        m_ajouterFrame->setEnabled(false);
        m_dupliquerFrame->setEnabled(false);
        m_copierFrame->setEnabled(false);
        m_collerFrame->setEnabled(false);
        m_boutonActuel->setText(QString("Sélection actuelle: %1").arg(m_selectionActuelle));
    }
    else
    {
        QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(m_zoneCentrale->activeSubWindow());
        m_page = tempWindow->page();
        m_pageEffets = tempWindow->pageEffets();
        m_num = tempWindow->num();

        emit s_rafraichirSelection(m_page, m_pageEffets, m_num);
    }
}

void FenetreEdition::h_update_rafraichirSelection(int fMax)
{
    QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(m_zoneCentrale->activeSubWindow());
    int gridNumX(0), gridNumY(0);
    if (m_num < 64)
    {
        gridNumX = 1+(m_num%8);
        gridNumY = 1+(m_num/8);
    }
    else if (m_num > 63 && m_num <72)
    {
        gridNumX = 0;
        gridNumY = m_num-63;
    }
    else if (m_num > 71 && m_num <80)
    {
        gridNumX = m_num-71;
        gridNumY = 9;
    }
    QString title = QString("Bouton %1x%2 | Page %3 | Page d'effets %4").arg(gridNumX).arg(gridNumY).arg(m_page).arg(m_pageEffets);
    m_selectionActuelle = title;
    if(fMax < 2 || m_frameActuelle->value() == 1)
    {
        m_supprimerFrame->setEnabled(false);
    }
    else m_supprimerFrame->setEnabled(true);
    m_frameActuelle->setMaximum(fMax);
    m_frameActuelle->setEnabled(true);
    m_dureeFrame->setEnabled(true);
    m_ajouterFrame->setEnabled(true);
    m_dupliquerFrame->setEnabled(true);
    m_copierFrame->setEnabled(true);
    if(m_clipboardRempli)
    {
        m_collerFrame->setEnabled(true);
    }
    else m_collerFrame->setEnabled(false);
    m_frameActuelle->setValue(tempWindow->prevPos());
    m_boutonActuel->setText(QString("Sélection actuelle: %1").arg(m_selectionActuelle));

    emit chargerFrame(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::h_updateEdition(const std::vector<int> &liste, int duree)
{
    QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(m_zoneCentrale->activeSubWindow());
    int id = tempWindow->ID();

    m_dureeFrame->setValue(duree);
    int j(0);
    for (int i=id*80; i<(id+1)*80; i++)
    {
        int selectionCouleur = liste[j];
        int idBtn = m_listeBouton[i]->number();
        if (selectionCouleur == 0)
        {
            if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72.png"));
            }
            else if (idBtn == 27){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172.png"));
            }
            else if (idBtn == 28){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272.png"));
            }
            else if (idBtn == 35){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372.png"));
            }
            else if (idBtn == 36){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472.png"));
            }
            else if (idBtn>63 && idBtn<80){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
            }
        }
        else
        {
            if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72c.png"));
            }
            else if (idBtn == 27){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172c.png"));
            }
            else if (idBtn == 28){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272c.png"));
            }
            else if (idBtn == 35){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372c.png"));
            }
            else if (idBtn == 36){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472c.png"));
            }
            else if (idBtn>63 && idBtn<80){
                m_listeBouton[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72c.png"));
            }
        }
        QGraphicsColorizeEffect *couleur = new QGraphicsColorizeEffect(m_bouton);
        QColor couleurActuelle(255, 255, 255);
        switch (selectionCouleur)
        {
        case 0:
            couleurActuelle = QColor(255, 255, 255);
            couleur->setStrength(0.0);
            break;
        case 1:
            couleurActuelle = QColor(0, 190, 0);
            couleur->setStrength(1.0);
            break;
        case 2:
            couleurActuelle = QColor(0, 190, 0);
            couleur->setStrength(0.8);
            break;
        case 3:
            couleurActuelle = QColor(0, 190, 0);
            couleur->setStrength(0.6);
            break;
        case 4:
            couleurActuelle = QColor(120, 255, 80);
            couleur->setStrength(1.0);
            break;
        case 5:
            couleurActuelle = QColor(120, 255, 80);
            couleur->setStrength(0.8);
            break;
        case 6:
            couleurActuelle = QColor(120, 255, 80);
            couleur->setStrength(0.6);
            break;
        case 7:
            couleurActuelle = QColor(255, 255, 0);
            couleur->setStrength(1.0);
            break;
        case 8:
            couleurActuelle = QColor(255, 255, 0);
            couleur->setStrength(0.8);
            break;
        case 9:
            couleurActuelle = QColor(255, 100, 0);
            couleur->setStrength(1.0);
            break;
        case 10:
            couleurActuelle = QColor(255, 100, 0);
            couleur->setStrength(0.8);
            break;
        case 11:
            couleurActuelle = QColor(255, 100, 0);
            couleur->setStrength(0.6);
            break;
        case 12:
            couleurActuelle = QColor(255, 100, 0);
            couleur->setStrength(0.4);
            break;
        case 13:
            couleurActuelle = QColor(255, 0, 0);
            couleur->setStrength(1.0);
            break;
        case 14:
            couleurActuelle = QColor(255, 0, 0);
            couleur->setStrength(0.8);
            break;
        case 15:
            couleurActuelle = QColor(255, 0, 0);
            couleur->setStrength(0.6);
            break;
        default:
            couleurActuelle = QColor(255, 255, 255);
            couleur->setStrength(0.0);
        }
        couleur->setColor(couleurActuelle);
        m_listeBouton[i]->setGraphicsEffect(couleur);
        j++;
    }
}

void FenetreEdition::onm_ajouterFramePressed()
{
    emit ajouterFramePressed(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::h_update_ajouterFrame(int frameCounter)
{
    m_supprimerFrame->setEnabled(true);
    m_frameActuelle->setMaximum(frameCounter);
    m_frameActuelle->setValue(m_frameActuelle->value()+1);
}

void FenetreEdition::onm_supprimerFramePressed()
{
    emit supprimerFramePressed(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::h_update_supprimerFrame(int fMax)
{
    m_frameActuelle->setMaximum(fMax);
    if(m_frameActuelle->value()==1)
    {
        m_supprimerFrame->setEnabled(false);
    }
    else
    {
        m_supprimerFrame->setEnabled(true);
    }
    emit chargerFrame(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::onm_dupliquerFramePressed()
{
    emit dupliquerFramePressed(m_page, m_pageEffets, m_num, m_frameActuelle->value(), m_dureeFrame->value());
}

void FenetreEdition::h_update_dupliquerFrame(int fMax)
{
    m_supprimerFrame->setEnabled(true);
    m_frameActuelle->setMaximum(fMax);
    m_frameActuelle->setValue(m_frameActuelle->value()+1);
}

void FenetreEdition::onm_copierFramePressed()
{
    emit copierFramePressed(m_page, m_pageEffets, m_num, m_frameActuelle->value(), m_dureeFrame->value());
}

void FenetreEdition::h_update_copierFrame()
{
    m_collerFrame->setEnabled(true);
    m_clipboardRempli = true;
}

void FenetreEdition::onm_collerFramePressed()
{
    emit collerFramePressed(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::h_update_collerFrame()
{
    emit chargerFrame(m_page, m_pageEffets, m_num, m_frameActuelle->value());
}

void FenetreEdition::onm_dureeFrameValueChanged()
{
    emit dureeFrameValueChanged(m_page, m_pageEffets, m_num, m_frameActuelle->value(), m_dureeFrame->value());
}

void FenetreEdition::h_supprimerPage(int page, int pageEffets, int num)
{
    QList<QMdiSubWindow *> listeOnglets = m_zoneCentrale->subWindowList(QMdiArea::CreationOrder);
    for(int i=0; i<(int)listeOnglets.size(); i++)
    {
        QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(listeOnglets[i]);
        if(tempWindow->page() == page && tempWindow->pageEffets() == pageEffets && tempWindow->num() == num && tempWindow->windowTitle() != "Closed Window")
        {
            listeOnglets[i]->close();
            break;
        }
    }
    emit update_supprimerPage(page, pageEffets, num);
}

void FenetreEdition::h_supprimerPageB(int page, int pageEffets, int num)
{
    QList<QMdiSubWindow *> listeOnglets = m_zoneCentrale->subWindowList(QMdiArea::CreationOrder);
    for(int i=0; i<(int)listeOnglets.size(); i++)
    {
        QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(listeOnglets[i]);
        if(tempWindow->page() == page && tempWindow->pageEffets() == pageEffets && tempWindow->num() == num && tempWindow->windowTitle() != "Closed Window")
        {
            listeOnglets[i]->close();
            break;
        }
    }
    emit update_supprimerPageB(page, pageEffets, num);
}

void FenetreEdition::h_supprimerPageC(int page, int pageEffets, int num)
{
    QList<QMdiSubWindow *> listeOnglets = m_zoneCentrale->subWindowList(QMdiArea::CreationOrder);
    for(int i=0; i<(int)listeOnglets.size(); i++)
    {
        QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(listeOnglets[i]);
        if(tempWindow->page() == page && tempWindow->pageEffets() == pageEffets && tempWindow->num() == num && tempWindow->windowTitle() != "Closed Window")
        {
            listeOnglets[i]->close();
            break;
        }
    }
    emit update_supprimerPageC(page, pageEffets, num);
}

void FenetreEdition::onSubWindowClose()
{
    if (m_zoneCentrale->activeSubWindow() == 0 || m_zoneCentrale->activeSubWindow()->windowTitle() == QString("Closed Window"))
    {
        m_selectionActuelle = QString("Aucune");
        m_frameActuelle->setValue(1);
        m_frameActuelle->setMaximum(1);
        m_frameActuelle->setEnabled(false);
        m_dureeFrame->setValue(50);
        m_dureeFrame->setEnabled(false);
        m_ajouterFrame->setEnabled(false);
        m_dupliquerFrame->setEnabled(false);
        m_copierFrame->setEnabled(false);
        m_collerFrame->setEnabled(false);
        m_boutonActuel->setText(QString("Sélection actuelle: %1").arg(m_selectionActuelle));
    }
    else
    {
        QMdiSubWindowMod *tempWindow = static_cast<QMdiSubWindowMod*>(m_zoneCentrale->activeSubWindow());
        m_page = tempWindow->page();
        m_pageEffets = tempWindow->pageEffets();
        m_num = tempWindow->num();

        emit s_rafraichirSelection(m_page, m_pageEffets, m_num);
    }
}

void FenetreEdition::enregistrer(QString cheminFichier)
{
    ofstream fluxEcriture(cheminFichier.toStdString().c_str());
    if(fluxEcriture)
    {
        fluxEcriture << "inlets=2;outlets=3;var note=0,velocity=0,outNote=[],outColor=[],noteLength=[],page=1,decay=[],isRunning=[],currentNoteFrame=[],currentNoteLoop=[],noteBuffer=[],k=[],kBuffer=[],sendNote=new Task(fSendNote,this),indexOf=function(a){indexOf=\"function\"===typeof Array.prototype.indexOf?Array.prototype.indexOf:function(a){for(var b=-1,c=-1,b=0;b<this.length;b++)if(this[b]===a){c=b;break}return c};return indexOf.call(this,a)};" << endl;
        fluxEcriture << "function msg_int(a){if(0==inlet&&a!=note||1==inlet&&a!=velocity)if(0==inlet&&(note=a),1==inlet&&(velocity=a),0!=velocity&&0!=note){post(\"in\",\"-\",\"note:\",note,\"velocity:\",velocity,\"\\n\");outNote=[];outColor=[];noteLength=[];decay=[];switch(page){";
        for (int page=1; page<9; page++)
        {
            /*for (int i=0; i<(int)m_listeFrame->size(); i++)
            {
                ContainerFrame *tempFrame = (*m_listeFrame)[i];
                if (tempFrame->existe(page))
                {
                    fluxEcriture << "case " << page << ":switch(note){";
                    for (int j=0; j<(int)m_listeFrame->size(); j++)
                    {
                        ContainerFrame *tempFrameB = (*m_listeFrame)[j];
                    }
                }
                break;
            }*/
        }
    }
}
        /*for (int p=1; p<9; p++)
        {
            for (int f=0; f<(int)m_listeFrame->size(); f++)
            {
                ContainerFrame *tempFrame = (*m_listeFrame)[f];
                if (tempFrame->existe(p))
                {
                    fluxEcriture << "case " << pageAct << ":switch(note){";
                    /*for (int frameActB=0; frameActB<(int)m_listeFrame->size(); frameActB++)
                    {
                        ContainerFrame *tempFrameB = (*m_listeFrame)[frameActB];
                        for (int numAct=0; numAct<80; numAct++)
                        {
                            if (tempFrameB->existe(pageAct, -1, numAct)) //Si la page contient un édit sur un bouton
                            {
                                int nbPageEff(0); //Calcul du nombre de pages à rendre
                                int pageEffCache(0);
                                for (int pageEffAct=1; pageEffAct<17; pageEffAct++)
                                {
                                    if (tempFrame->existe(frameActB, pageEffAct, numAct))
                                    {
                                        nbPageEff++;
                                        pageEffCache = pageEffAct;
                                    }
                                    if (nbPageEff>1)
                                    {
                                        break;
                                    }
                                }
                                if (nbPageEff == 1) //Si il n'y a qu'une page à rendre
                                {
                                    fluxEcriture << "case " << numAct << ":outNote.push(";
                                    QString outNote("");
                                    for (int frameActC=0; frameActC<(int)m_listeFrame->size(); frameActC++)
                                    {
                                        ContainerFrame *tempFrameC = (*m_listeFrame)[frameActC];
                                        int frameActD(1);
                                        do
                                        {
                                            if (tempFrame->existe(pageAct, pageEffCache, numAct, frameActD))
                                            {
                                                int *listeBouton = tempFrame->listeBouton();
                                                for (int note=0; note<80; note++)
                                                {
                                                    if (listeBouton[note] != 0)
                                                    {
                                                        outNote += noteToMidi(note) + ",";
                                                    }
                                                }
                                            }
                                            frameActD++;
                                        } while (tempFrameC->page() == pageAct);
                                    }
                                    outNote.section(",", 0, ((int)outNote.size()/3)-1);
                                    fluxEcriture << outNote.toStdString() << "),outColor.push(";

                                    //outNote.push(36,37,38,39,40,41,42,43),outColor.push(60,
                                    //79,60,79,60,79,60,79),noteLength.push(125,125,125,125,125,125,125,125),decay.push(100,100,100,100,100,100,100,100)}}
                                    fluxEcriture << "break;";
                                }
                                /*else
                                {
                                    for (int l=1; l<17; l++)
                                    {
                                        if (tempFrame->existe(j, l, k))
                                        {

                                        }
                                    }
                                }*/
                                /*break;
                                //
                            } else if (tempFrameB->page() != pageAct) break;
                        } break;
                    }
                } else if (tempFrame->page() != pageAct) break;*/
        /*for (int pageActuelle=1; pageActuelle<9; pageActuelle++)
        {
            for (int frameActuelle=0; frameActuelle<(int)m_listeFrame->size(); frameActuelle++)
            {
                ContainerFrame *tempFrame = (*m_listeFrame)[frameActuelle];
                if (tempFrame->existe(pageActuelle))
                {
                    fluxEcriture << "case " << pageActuelle << ":switch(note){";
                    for (int frameActuelleBis=0; frameActuelleBis<(int)m_listeFrame->size(); frameActuelleBis++)
                    {
                        ContainerFrame *tempFrame = (*m_listeFrame)[frameActuelleBis];
                        for (int numActuel=0; numActuel<80; numActuel++)
                        {
                            if (tempFrame->existe(pageActuelle, -1, numActuel))
                            {
                                int nbPageEff(0);
                                int pageEffCache(0);
                                for (int pageEffActuelle=1; pageEffActuelle<17; pageEffActuelle++)
                                {
                                    if (tempFrame->existe(frameActuelleBis, pageEffActuelle, numActuel))
                                    {
                                        nbPageEff++;
                                        pageEffCache = pageEffActuelle;
                                    }
                                    if (nbPageEff>1)
                                    {
                                        break;
                                    }
                                }
                                if (nbPageEff == 1)
                                {
                                    fluxEcriture << "case " << numActuel << ":outNote.push(";
                                    QString outNote("");
                                    for (int p=0; p<(int)m_listeFrame->size(); p++)
                                    {
                                        ContainerFrame *tempFrame = (*m_listeFrame)[p];
                                        int s(1);
                                        bool t(false);
                                        do
                                        {
                                            if (tempFrame->existe(j, q, k, s))
                                            {
                                                int *listeBouton = tempFrame->listeBouton();
                                                for (int n=0; n<80; n++)
                                                {
                                                    if (listeBouton[n] != 0)
                                                    {
                                                        switch(n)
                                                        {
                                                        case 0:
                                                            outNote += "64,";
                                                            break;
                                                        case 1:
                                                            outNote += "65,";
                                                            break;
                                                        case 2:
                                                            outNote += "66,";
                                                            break;
                                                        case 3:
                                                            outNote += "67,";
                                                            break;
                                                        case 4:
                                                            outNote += "96,";
                                                            break;
                                                        case 5:
                                                            outNote += "97,";
                                                            break;
                                                        case 6:
                                                            outNote += "98,";
                                                            break;
                                                        case 7:
                                                            outNote += "99,";
                                                            break;
                                                        case 8:
                                                            outNote += "60,";
                                                            break;
                                                        case 9:
                                                            outNote += "61,";
                                                            break;
                                                        case 10:
                                                            outNote += "62,";
                                                            break;
                                                        case 11:
                                                            outNote += "63,";
                                                            break;
                                                        case 12:
                                                            outNote += "92,";
                                                            break;
                                                        case 13:
                                                            outNote += "93,";
                                                            break;
                                                        case 14:
                                                            outNote += "94,";
                                                            break;
                                                        case 15:
                                                            outNote += "95,";
                                                            break;
                                                        case 16:
                                                            outNote += "56,";
                                                            break;
                                                        case 17:
                                                            outNote += "57,";
                                                            break;
                                                        case 18:
                                                            outNote += "58,";
                                                            break;
                                                        case 19:
                                                            outNote += "59,";
                                                            break;
                                                        case 20:
                                                            outNote += "88,";
                                                            break;
                                                        case 21:
                                                            outNote += "89,";
                                                            break;
                                                        case 22:
                                                            outNote += "90,";
                                                            break;
                                                        case 23:
                                                            outNote += "91,";
                                                            break;
                                                        case 24:
                                                            outNote += "52,";
                                                            break;
                                                        case 25:
                                                            outNote += "53,";
                                                            break;
                                                        case 26:
                                                            outNote += "54,";
                                                            break;
                                                        case 27:
                                                            outNote += "55,";
                                                            break;
                                                        case 28:
                                                            outNote += "84,";
                                                            break;
                                                        case 29:
                                                            outNote += "85,";
                                                            break;
                                                        case 30:
                                                            outNote += "86,";
                                                            break;
                                                        case 31:
                                                            outNote += "87,";
                                                            break;
                                                        case 32:
                                                            outNote += "48,";
                                                            break;
                                                        case 33:
                                                            outNote += "49,";
                                                            break;
                                                        case 34:
                                                            outNote += "50,";
                                                            break;
                                                        case 35:
                                                            outNote += "51,";
                                                            break;
                                                        case 36:
                                                            outNote += "80,";
                                                            break;
                                                        case 37:
                                                            outNote += "81,";
                                                            break;
                                                        case 38:
                                                            outNote += "82,";
                                                            break;
                                                        case 39:
                                                            outNote += "83,";
                                                            break;
                                                        case 40:
                                                            outNote += "44,";
                                                            break;
                                                        case 41:
                                                            outNote += "45,";
                                                            break;
                                                        case 42:
                                                            outNote += "46,";
                                                            break;
                                                        case 43:
                                                            outNote += "47,";
                                                            break;
                                                        case 44:
                                                            outNote += "76,";
                                                            break;
                                                        case 45:
                                                            outNote += "77,";
                                                            break;
                                                        case 46:
                                                            outNote += "78,";
                                                            break;
                                                        case 47:
                                                            outNote += "79,";
                                                            break;
                                                        case 48:
                                                            outNote += "40,";
                                                            break;
                                                        case 49:
                                                            outNote += "41,";
                                                            break;
                                                        case 50:
                                                            outNote += "42,";
                                                            break;
                                                        case 51:
                                                            outNote += "43,";
                                                            break;
                                                        case 52:
                                                            outNote += "72,";
                                                            break;
                                                        case 53:
                                                            outNote += "73,";
                                                            break;
                                                        case 54:
                                                            outNote += "74,";
                                                            break;
                                                        case 55:
                                                            outNote += "75,";
                                                            break;
                                                        case 56:
                                                            outNote += "36,";
                                                            break;
                                                        case 57:
                                                            outNote += "37,";
                                                            break;
                                                        case 58:
                                                            outNote += "38,";
                                                            break;
                                                        case 59:
                                                            outNote += "39,";
                                                            break;
                                                        case 60:
                                                            outNote += "68,";
                                                            break;
                                                        case 61:
                                                            outNote += "69,";
                                                            break;
                                                        case 62:
                                                            outNote += "70,";
                                                            break;
                                                        case 63:
                                                            outNote += "71,";
                                                            break;
                                                        }
                                                    }
                                                }
                                                t = true;
                                            }
                                            s++;
                                        } while (t == false);
                                    }
                                    outNote.section(",", 0, ((int)outNote.size()/3)-1);
                                    fluxEcriture << outNote.toStdString() << "),outColor.push(";

                                    //outNote.push(36,37,38,39,40,41,42,43),outColor.push(60,
                                    //79,60,79,60,79,60,79),noteLength.push(125,125,125,125,125,125,125,125),decay.push(100,100,100,100,100,100,100,100)}}
                                    fluxEcriture << "break;";
                                }
                                else
                                {
                                    for (int l=1; l<17; l++)
                                    {
                                        if (tempFrame->existe(j, l, k))
                                        {

                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break; //la page est déjà écrit
                }
            }
        }
    }
    else
    {
        QMessageBox::critical(this, "Erreur", "Écriture impossible.");
    }*/

bool FenetreEdition::trier(const ContainerFrame& firstFrame, const ContainerFrame& secondFrame)
{
    /*return firstFrame.page()<secondFrame.page() ||
           (firstFrame.page()==secondFrame.page() && firstFrame.pageEffets()<secondFrame.pageEffets()) ||
           (firstFrame.page()==secondFrame.page() && firstFrame.pageEffets()==secondFrame.pageEffets() && firstFrame.frame()<secondFrame.frame());*/
    return std::forward_as_tuple(firstFrame.page(), firstFrame.pageEffets(), firstFrame.frame())
         < std::forward_as_tuple(secondFrame.page(), secondFrame.pageEffets(), secondFrame.frame());
}

void FenetreEdition::h_erreur(QString message)
{
    QMessageBox::critical(this, "Erreur", "Une erreur est survenue:\n\n" + message);
}

void FenetreEdition::getPtrFenetreSelection(FenetreSelection *fen)
{
    m_ptrFenetreSelection = fen;
    QObject::connect(m_ptrFenetreSelection, SIGNAL(chargerPage(int,int)), m_mainFWorker, SLOT(h_chargerPage(int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(updateSelection(std::vector<bool>)), m_ptrFenetreSelection, SLOT(h_updateSelection(std::vector<bool>)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(copierPage(int,int,int)), m_mainFWorker, SLOT(h_copierPage(int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(collerPage(int,int,int)), m_mainFWorker, SLOT(h_collerPage(int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(supprimerPageB(int,int,int)), this, SLOT(h_supprimerPageB(int,int,int)), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(update_supprimerPageB(int,int,int)), m_mainFWorker, SLOT(h_update_supprimerPageB(int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(supprimerPage(int,int,int)), this, SLOT(h_supprimerPage(int,int,int)), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(update_supprimerPage(int,int,int)), m_mainFWorker, SLOT(h_update_supprimerPage(int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(ouvrirOnglet(int,int,int,int)), this, SLOT(h_ouvrirOnglet(int,int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(supprimerPatDir(QString,bool)), m_mainFWorker, SLOT(h_supprimerPatDir(QString,bool)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(savePattern(int,int,int,QString)), m_mainFWorker, SLOT(h_savePattern(int,int,int,QString)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(loadPattern(int,int,int,QString)), m_mainFWorker, SLOT(h_loadPattern(int,int,int,QString)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(supprimerPageC(int,int,int)), this, SLOT(h_supprimerPageC(int,int,int)), Qt::QueuedConnection);
    QObject::connect(this, SIGNAL(update_supprimerPageC(int,int,int)), m_mainFWorker, SLOT(h_update_supprimerPageC(int,int,int)), Qt::QueuedConnection);
    QObject::connect(m_ptrFenetreSelection, SIGNAL(enregistrer(QString)), m_mainFWorker, SLOT(h_enregistrer(QString)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(initProgress(int)), this, SLOT(h_initProgress(int)), Qt::QueuedConnection);
    QObject::connect(m_mainFWorker, SIGNAL(closeProgress()), this, SLOT(h_closeProgress()), Qt::QueuedConnection);
}

void FenetreEdition::h_initProgress(int max)
{
    m_progress = new QProgressDialog("Progression...", "Annuler", 0, max);
    QObject::connect(m_mainFWorker, SIGNAL(showProgress(int)), m_progress, SLOT(setValue(int)), Qt::QueuedConnection);
    m_progress->exec();
}
void FenetreEdition::h_closeProgress()
{
    QObject::disconnect(m_mainFWorker, SIGNAL(showProgress(int)), m_progress, SLOT(setValue(int)));
    delete m_progress;
}
