#ifndef CONTAINERFRAME_H
#define CONTAINERFRAME_H

#include <vector>


class ContainerFrame
{
public:
    ContainerFrame(const int page, const int pageEffets, const int num, int frame, int duree);
    std::vector< int > listeBouton();
    bool const existe(int page, int pageEffets = -1, int num = -1, int frame = -1);
    void setDuree(int duree);
    void setFrame(int frame);
    void setListeBouton(std::vector< int > listeBouton);
    int duree() const;
    int frame() const;
    int page() const;
    int pageEffets() const;
    int num() const;
    void ajouterSupprBouton(int bouton, int couleur);
    ~ContainerFrame();
private:
    const int m_page;
    const int m_pageEffets;
    const int m_num;
    int m_frame;
    int m_duree;
    std::vector< int > m_listeBouton;
};

#endif // CONTAINERFRAME_H
