#include "fenetreselection.h"


using namespace std;

FenetreSelection::FenetreSelection(QWidget *parent) : QMainWindow(parent)
{
    m_lwDir = QString("C:/Users/Florent/Documents/Lightworks/" + tr("Mes effets prédéfinis"));
    if (!(m_lwDir.exists()))
    {
        if (!(m_lwDir.mkpath(m_lwDir.absolutePath())))
        {
            emit h_erreur("Impossible de créer le dossier de prédéfinis");
        }
    }
    QMenu *menuFichier = menuBar()->addMenu(tr("&Fichier"));
    QAction *actionImporter = new QAction(tr("&Importer"), this);
    menuFichier->addAction(actionImporter);
    QAction *actionImporterPP = new QAction(tr("Importer depuis le presse-papier"), this);
    menuFichier->addAction(actionImporterPP);
    QAction *actionEnregistrer = new QAction(tr("Enregi&strer"), this);
    menuFichier->addAction(actionEnregistrer);
    connect(actionEnregistrer, SIGNAL(triggered()), this, SLOT(onActionEnregistrer()));
    QAction *actionEnregistrerSous = new QAction(tr("E&nregistrer sous..."), this);
    menuFichier->addAction(actionEnregistrerSous);
    connect(actionEnregistrerSous, SIGNAL(triggered()), this, SLOT(onActionEnregistrerSous()));
    QAction *actionExporter = new QAction(tr("Exporter vers le presse-papier"), this);
    menuFichier->addAction(actionExporter);
    menuFichier->addSeparator();
    QAction *actionQuitter = new QAction(tr("&Quitter"), this);
    menuFichier->addAction(actionQuitter);
    connect(actionQuitter, SIGNAL(triggered()), qApp, SLOT(quit()));

    QWidget *zoneCentrale = new QWidget;
    m_listeBouton = new vector< QClickableLabel* >;

    QComboBox *modele = new QComboBox;
    modele->addItem("Launchpad S");
    modele->addItem("Launchpad Mk2");
    m_modele = 0;
    QObject::connect(modele, SIGNAL(currentIndexChanged(int)), this, SLOT(onModeleSelectionChanged(int)), Qt::QueuedConnection);

    m_page = new QSpinBox;
    m_page->setMinimum(1);
    m_page->setMaximum(8);
    m_page->setPrefix(tr("Page "));
    QObject::connect(m_page, SIGNAL(valueChanged(int)), this, SLOT(onm_pageValueChanged()), Qt::QueuedConnection);

    m_pageEffets = new QSpinBox;
    m_pageEffets->setMinimum(1);
    m_pageEffets->setMaximum(16);
    m_pageEffets->setPrefix(tr("Page d'effets "));
    QObject::connect(m_pageEffets, SIGNAL(valueChanged(int)), this, SLOT(onm_pageEffetsValueChanged()), Qt::QueuedConnection);

    QGroupBox *mode = new QGroupBox;
    mode->setTitle(tr("Sélection"));
    m_editer = new QPushButton(tr("Éditer"));
    m_editer->setCheckable(true);
    m_editer->setChecked(true);
    m_editer->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/edit.png"));
    m_mode = 0;
    QObject::connect(m_editer, SIGNAL(clicked()), this, SLOT(onm_editerClicked()), Qt::QueuedConnection);
    m_supprimer = new QPushButton(tr("Supprimer"));
    m_supprimer->setCheckable(true);
    m_supprimer->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/remove.png"));
    QObject::connect(m_supprimer, SIGNAL(clicked()), this, SLOT(onm_supprimerClicked()), Qt::QueuedConnection);
    m_copier = new QPushButton(tr("Copier"));
    m_copier->setCheckable(true);
    m_copier->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/copy.png"));
    QObject::connect(m_copier, SIGNAL(clicked()), this, SLOT(onm_copierClicked()), Qt::QueuedConnection);
    m_coller = new QPushButton(tr("Coller"));
    m_coller->setCheckable(true);
    m_coller->setEnabled(false);
    m_coller->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/paste.png"));
    QObject::connect(m_coller, SIGNAL(clicked()), this, SLOT(onm_collerClicked()), Qt::QueuedConnection);
    QHBoxLayout *modeLayout = new QHBoxLayout;
    modeLayout->addWidget(m_editer);
    modeLayout->addWidget(m_supprimer);
    modeLayout->addWidget(m_copier);
    modeLayout->addWidget(m_coller);
    mode->setLayout(modeLayout);

    QGroupBox *patternButton = new QGroupBox;
    patternButton->setTitle(tr("Prédéfini"));
    m_sauverPattern = new QPushButton("Sauver");
    m_sauverPattern->setCheckable(true);
    m_sauverPattern->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/save.png"));
    QObject::connect(m_sauverPattern, SIGNAL(clicked()), this, SLOT(onm_sauverPatternClicked()), Qt::QueuedConnection);
    m_collerPattern = new QPushButton("Coller");
    m_collerPattern->setCheckable(true);
    m_collerPattern->setEnabled(false);
    m_collerPattern->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/load.png"));
    QObject::connect(m_collerPattern, SIGNAL(clicked()), this, SLOT(onm_collerPatternClicked()), Qt::QueuedConnection);
    QHBoxLayout *upPatternButton = new QHBoxLayout;
    upPatternButton->addWidget(m_sauverPattern);
    upPatternButton->addWidget(m_collerPattern);
    patternButton->setLayout(upPatternButton);

    QDockWidget *mDock = new QDockWidget;
    addDockWidget(Qt::RightDockWidgetArea, mDock);
    mDock->setWindowTitle(tr("Sélection de prédéfinis"));
    mDock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    mDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    QVBoxLayout *dockLayout = new QVBoxLayout;
    QDir rootPath(m_lwDir);
    rootPath.cdUp();
    m_dockModele = new MyFileSystemModel;
    m_dockModele->setRootPath(rootPath.absolutePath());
    m_dockModele->setNameFilters(QStringList()<<"*.lwe");
    m_dockModele->setNameFilterDisables(false);
    m_dockModele->setReadOnly(false);
    m_dockModele->setFilter(QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot);
    m_dockModele->setHeaderData(0, Qt::Horizontal, "Name", Qt::DisplayRole);
    m_vue = new QTreeView;
    m_vue->setModel(m_dockModele);
    m_vue->setRootIndex(m_dockModele->index(rootPath.absolutePath()));
    m_vue->hideColumn(1);
    m_vue->hideColumn(2);
    m_vue->hideColumn(3);
    dockLayout->addWidget(m_vue);
    QObject::connect(m_vue, SIGNAL(clicked(QModelIndex)), this, SLOT(onm_vueClicked()), Qt::QueuedConnection);
    m_importerPattern = new QPushButton(tr("Importer"));
    m_importerPattern->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/import.png"));
    m_exporterPattern = new QPushButton(tr("Exporter"));
    m_exporterPattern->setEnabled(false);
    m_exporterPattern->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/export.png"));
    m_supprimerPatDir = new QPushButton(tr("Supprimer"));
    m_supprimerPatDir->setEnabled(false);
    m_supprimerPatDir->setIcon(QPixmap(QDir::currentPath() + "/Lightworks Ressources/remove.png"));
    QObject::connect(m_supprimerPatDir, SIGNAL(clicked()), this, SLOT(onm_supprimerPatDirClicked()), Qt::QueuedConnection);
    QHBoxLayout *patternButtonL = new QHBoxLayout;
    patternButtonL->addWidget(m_importerPattern);
    patternButtonL->addWidget(m_exporterPattern);
    patternButtonL->addWidget(m_supprimerPatDir);
    QWidget *conteneurPatternButton = new QWidget;
    conteneurPatternButton->setLayout(patternButtonL);
    dockLayout->addWidget(conteneurPatternButton);
    QWidget *conteneurDock = new QWidget;
    conteneurDock->setLayout(dockLayout);
    mDock->setWidget(conteneurDock);

    //QLabel *background = new QLabel(zoneCentrale);
    QGridLayout *layout = new QGridLayout(zoneCentrale);
    //background->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/bg.png"));
    for (int i = 0; i<80; i++) {
        m_bouton = new QClickableLabel("", zoneCentrale, i);
        if (i != 27 && i != 28 && i != 35 && i != 36 && i<64){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72.png"));
            layout->addWidget(m_bouton, 1+(i/8), i%8);
        }
        else if (i == 27){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172.png"));
            layout->addWidget(m_bouton, 1+(i/8), i%8);
        }
        else if (i == 28){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272.png"));
            layout->addWidget(m_bouton, 1+(i/8), i%8);
        }
        else if (i == 35){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372.png"));
            layout->addWidget(m_bouton, 1+(i/8), i%8);
        }
        else if (i == 36){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472.png"));
            layout->addWidget(m_bouton, 1+(i/8), i%8);
        }
        else if (i>63 && i<72){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
            layout->addWidget(m_bouton, 0, i-64);
        }
        else if (i>71 && i<80){
            m_bouton->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
            layout->addWidget(m_bouton, i-71, 9);
        }
        connect(m_bouton, SIGNAL(clicked()), SLOT(onButtonClicked()));
        m_listeBouton->push_back(m_bouton);
    }
    QLabel *logo = new QLabel("");
    logo->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/logo.png"));
    layout->addWidget(logo, 0, 9);
    zoneCentrale->setLayout(layout);

    QHBoxLayout *toolBoxLayout1 = new QHBoxLayout;
    QHBoxLayout *toolBoxLayout2 = new QHBoxLayout;
    toolBoxLayout1->addWidget(modele);
    toolBoxLayout1->addWidget(m_page);
    toolBoxLayout1->addWidget(m_pageEffets);
    toolBoxLayout2->addWidget(mode);
    toolBoxLayout2->addWidget(patternButton);
    QWidget *conteneurToolBox1 = new QWidget;
    conteneurToolBox1->setLayout(toolBoxLayout1);
    QWidget *conteneurToolBox2 = new QWidget;
    conteneurToolBox2->setLayout(toolBoxLayout2);
    setCentralWidget(zoneCentrale);

    QToolBar *mainToolBar = addToolBar("Principale");
    mainToolBar->addWidget(conteneurToolBox1);
    mainToolBar->addSeparator();
    mainToolBar->addWidget(conteneurToolBox2);
    mainToolBar->setMovable(false);

    this->layout()->setSizeConstraint( QLayout::SetFixedSize );
    zoneCentrale->layout()->setSizeConstraint( QLayout::SetFixedSize );
    this->setWindowTitle(QString("Lightworks - ")+QString(tr("Fenêtre de sélection")));
}

FenetreSelection::~FenetreSelection()
{
}

void const FenetreSelection::onm_pageValueChanged() // slot
{
    m_page->findChild<QLineEdit*>()->deselect();
    emit chargerPage(m_page->value(), m_pageEffets->value());
}

void const FenetreSelection::onm_pageEffetsValueChanged() // slot
{
    m_pageEffets->findChild<QLineEdit*>()->deselect();
    emit chargerPage(m_page->value(), m_pageEffets->value());
}

void FenetreSelection::onModeleSelectionChanged(int modele)
{
    m_modele = modele;
}

void FenetreSelection::onm_editerClicked() // slot
{
    if (m_editer->isChecked() == false)
    {
        m_editer->setChecked(true);
    }
    m_mode = 0;
    m_supprimer->setChecked(false);
    m_copier->setChecked(false);
    m_coller->setChecked(false);
    m_sauverPattern->setChecked(false);
    m_collerPattern->setChecked(false);
}

void FenetreSelection::onm_supprimerClicked() // slot
{
    if (m_supprimer->isChecked() == false)
    {
        m_supprimer->setChecked(true);
    }
    m_mode = 1;
    m_editer->setChecked(false);
    m_copier->setChecked(false);
    m_coller->setChecked(false);
    m_sauverPattern->setChecked(false);
    m_collerPattern->setChecked(false);
}

void FenetreSelection::onm_copierClicked() // slot
{
    if (m_copier->isChecked() == false)
    {
        m_copier->setChecked(true);
    }
    m_mode = 2;
    m_editer->setChecked(false);
    m_supprimer->setChecked(false);
    m_coller->setChecked(false);
    m_sauverPattern->setChecked(false);
    m_collerPattern->setChecked(false);
}

void FenetreSelection::onm_collerClicked() // slot
{
    if (m_coller->isChecked() == false)
    {
        m_coller->setChecked(true);
    }
    m_mode = 3;
    m_editer->setChecked(false);
    m_supprimer->setChecked(false);
    m_copier->setChecked(false);
    m_sauverPattern->setChecked(false);
    m_collerPattern->setChecked(false);
}

void FenetreSelection::onm_sauverPatternClicked() // slot
{
    if (m_importerPattern->isChecked() == false)
    {
        m_importerPattern->setChecked(true);
    }
    m_mode = 4;
    m_editer->setChecked(false);
    m_supprimer->setChecked(false);
    m_copier->setChecked(false);
    m_coller->setChecked(false);
    m_collerPattern->setChecked(false);
}

void FenetreSelection::onm_collerPatternClicked() // slot
{
    if (m_exporterPattern->isChecked() == false)
    {
        m_exporterPattern->setChecked(true);
    }
    m_mode = 5;
    m_editer->setChecked(false);
    m_supprimer->setChecked(false);
    m_copier->setChecked(false);
    m_coller->setChecked(false);
    m_sauverPattern->setChecked(false);
}


void FenetreSelection::onButtonClicked() // slot
{
    QClickableLabel *boutonSender = qobject_cast<QClickableLabel *>( this->sender() );
    int idBtn = boutonSender->number();
    if (m_mode == 0)
    {
        if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72c.png"));
        }
        else if (idBtn == 27){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172c.png"));
        }
        else if (idBtn == 28){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272c.png"));
        }
        else if (idBtn == 35){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372c.png"));
        }
        else if (idBtn == 36){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472c.png"));
        }
        else if (idBtn>63 && idBtn<80){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72c.png"));
        }
        QGraphicsColorizeEffect *color = new QGraphicsColorizeEffect(boutonSender);
        color->setColor(QColor(255, 178, 0));
        color->setStrength(1.0);
        boutonSender->setGraphicsEffect(color);

        //m_ptrFenetreEdition->OuvrirUnOnglet(m_modele, m_page->value(), m_pageEffets->value(), num);
        emit ouvrirOnglet(m_modele, m_page->value(), m_pageEffets->value(), boutonSender->number());
    }
    else if (m_mode == 1)
    {
        if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72.png"));
        }
        else if (idBtn == 27){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172.png"));
        }
        else if (idBtn == 28){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272.png"));
        }
        else if (idBtn == 35){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372.png"));
        }
        else if (idBtn == 36){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472.png"));
        }
        else if (idBtn>63 && idBtn<80){
            boutonSender->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
        }
        QGraphicsColorizeEffect *color = new QGraphicsColorizeEffect(boutonSender);
        color->setColor(QColor(255, 255, 255));
        color->setStrength(0.0);
        boutonSender->setGraphicsEffect(color);

        emit supprimerPage(m_page->value(), m_pageEffets->value(), boutonSender->number());
    }
    else if (m_mode == 2)
    {
        m_coller->setEnabled(true);

        emit copierPage(m_page->value(), m_pageEffets->value(), boutonSender->number());
    }
    else if (m_mode == 3)
    {
        emit collerPage(m_page->value(), m_pageEffets->value(), boutonSender->number());
    }
    else if (m_mode == 4)
    {
        emit savePattern(m_page->value(), m_pageEffets->value(), boutonSender->number(), m_lwDir.absolutePath());
    }
    else if (m_mode == 5)
    {
        emit loadPattern(m_page->value(), m_pageEffets->value(), boutonSender->number(), m_currentPath);
    }
}

void FenetreSelection::onActionEnregistrer()
{
    if(m_sauvegarde == "")
    {
        onActionEnregistrerSous();
    }
    else
    {
        emit enregistrer(m_sauvegarde);
    }
}

void FenetreSelection::onActionEnregistrerSous()
{
    m_sauvegarde = QFileDialog::getSaveFileName(this, "Enregistrer sous...", QString(), "Javascript (*.js)");
    if(m_sauvegarde != "") onActionEnregistrer();
}

void FenetreSelection::onActionToClipboard()
{

}

void FenetreSelection::onActionCharger()
{
    QString cheminFichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Javascript (*.js)");
    if(cheminFichier != "")
    {
        //charger le fichier
    }
}

void FenetreSelection::onActionFromClipboard()
{

}

void FenetreSelection::h_updateSelection(const std::vector<bool> &liste)
{
    for (int i=0; i<80; i++)
    {
        QClickableLabel *tempBouton = (*m_listeBouton)[i];
        QGraphicsColorizeEffect *couleur = new QGraphicsColorizeEffect(tempBouton);
        int idBtn = tempBouton->number();
        if(liste[i])
        {
            if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72c.png"));
            }
            else if (idBtn == 27){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172c.png"));
            }
            else if (idBtn == 28){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272c.png"));
            }
            else if (idBtn == 35){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372c.png"));
            }
            else if (idBtn == 36){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472c.png"));
            }
            else if (idBtn>63 && idBtn<80){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72c.png"));
            }
            couleur->setStrength(1.0);
            couleur->setColor(QColor(255, 178, 0));
        }
        else
        {
            if (idBtn != 27 && idBtn != 28 && idBtn != 35 && idBtn != 36 && idBtn<64){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/button72.png"));
            }
            else if (idBtn == 27){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner172.png"));
            }
            else if (idBtn == 28){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner272.png"));
            }
            else if (idBtn == 35){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner372.png"));
            }
            else if (idBtn == 36){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttoncorner472.png"));
            }
            else if (idBtn>63 && idBtn<80){
                (*m_listeBouton)[i]->setPixmap(QPixmap(QDir::currentPath() + "/Lightworks Ressources/buttonrounded72.png"));
            }
            couleur->setStrength(0.0);
            couleur->setColor(QColor(255, 255, 255));
        }
        tempBouton->setGraphicsEffect(couleur);
    }
}

void FenetreSelection::onm_supprimerPatDirClicked()
{
    m_exporterPattern->setEnabled(false);
    m_supprimerPatDir->setEnabled(false);
    m_collerPattern->setEnabled(false);
    if (m_collerPattern->isChecked())
    {
        m_collerPattern->setChecked(false);
        m_mode = 4;
        m_sauverPattern->setChecked(true);
    }
    emit supprimerPatDir(m_currentPath, m_dockModele->isDir(m_vue->selectionModel()->currentIndex()));
    m_vue->selectionModel()->clearCurrentIndex();
}

void FenetreSelection::rafraichir()
{
    this->setEnabled(false);
    this->setEnabled(true);
}

void FenetreSelection::h_erreur(QString message)
{
    QMessageBox::critical(this, "Erreur", "Une erreur est survenue:\n\n" + message);
}

void FenetreSelection::onm_vueClicked()
{
    QItemSelectionModel *selection = m_vue->selectionModel();
    QModelIndex indexElementSelectionne = selection->currentIndex();
    //QVariant elementSelectionne = m_dockModele->data(indexElementSelectionne, Qt::DisplayRole);
    //m_currentPath = QString(elementSelectionne.toString());
    m_currentPath = m_dockModele->filePath(indexElementSelectionne);
    m_exporterPattern->setEnabled(true);
    if (m_dockModele->data(indexElementSelectionne, Qt::DisplayRole).toString() != "Mes effets prédéfinis")
    {
        m_supprimerPatDir->setEnabled(true);
    }
    else
    {
        m_supprimerPatDir->setEnabled(false);
    }
    if (m_dockModele->isDir(indexElementSelectionne))
    {
        m_collerPattern->setEnabled(false);
        if (m_collerPattern->isChecked())
        {
            m_collerPattern->setChecked(false);
            m_mode = 4;
            m_sauverPattern->setChecked(true);
        }
    }
    else
    {
        m_collerPattern->setEnabled(true);
    }
}
