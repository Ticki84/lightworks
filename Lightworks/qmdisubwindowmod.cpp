#include "qmdisubwindowmod.h"
#include "fenetreedition.h"


QMdiSubWindowMod::QMdiSubWindowMod(const int page, const int pageEffets, const int num, int id, QWidget * parent, Qt::WindowFlags flags)
    : QMdiSubWindow(parent, flags), m_page(page), m_pageEffets(pageEffets), m_num(num), m_ID(id)
{
}

QMdiSubWindowMod::~QMdiSubWindowMod()
{
}

void QMdiSubWindowMod::setID(int id)
{
    m_ID = id;
}

int const QMdiSubWindowMod::ID()
{
    return m_ID;
}

int const QMdiSubWindowMod::page()
{
    return m_page;
}

int const QMdiSubWindowMod::pageEffets()
{
    return m_pageEffets;
}

int const QMdiSubWindowMod::num()
{
    return m_num;
}

void QMdiSubWindowMod::setPrevPos(int pos)
{
    m_prevPos = pos;
}

int QMdiSubWindowMod::prevPos()
{
    return m_prevPos;
}

void QMdiSubWindowMod::closeEvent (QCloseEvent *event)
{
    setWindowTitle("Closed Window");
    m_ptrFenetreEdition->onSubWindowClose();
}

void QMdiSubWindowMod::getPtrFenetreEdition(FenetreEdition* fen)
{
    m_ptrFenetreEdition = fen;
}
