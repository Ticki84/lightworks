#ifndef FENETREEDITION_H
#define FENETREEDITION_H

#include "qclickablelabel.h"
#include "frameworker.h"
#include "qmdisubwindowmod.h"
#include <QtWidgets/QtWidgets>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <tuple>
#include <fenetreselection.h>


class FenetreEdition : public QMainWindow
{
        Q_OBJECT

public:
    explicit FenetreEdition();
    void onSubWindowClose();
    void enregistrer(QString cheminFichier);
    bool trier(const ContainerFrame& firstFrame, const ContainerFrame& secondFrame);
    void getPtrFenetreSelection(FenetreSelection* fen);
    ~FenetreEdition();
public slots:
    void onm_dureeFrameValueChanged();
    void const onm_frameActuelleValueChanged();
    void onm_ajouterFramePressed();
    void onm_supprimerFramePressed();
    void onm_dupliquerFramePressed();
    void onm_copierFramePressed();
    void onm_collerFramePressed();
    void rafraichirSelection();
    void onColorClicked();
    void onButtonClicked();
    void h_updateEdition(const std::vector<int> &liste, int duree);
    void h_erreur(QString eMessage);
    void h_update_ajouterFrame(int frameCounter);
    void h_update_supprimerFrame(int fMax);
    void h_update_dupliquerFrame(int fMax);
    void h_update_copierFrame();
    void h_update_collerFrame();
    void h_supprimerPage(int page,int pageEffets, int num);
    void h_supprimerPageB(int page,int pageEffets, int num);
    void h_supprimerPageC(int page,int pageEffets, int num);
    void h_update_rafraichirSelection(int fMax);
    void h_ouvrirOnglet(int modele, int page, int pageEffets, int num);
    void h_initProgress(int max);
    void h_closeProgress();
signals:
    void dureeFrameValueChanged(int page, int pageEffets, int num, int frameActuelle, int dureeFrame);
    void ajouterFramePressed(int page, int pageEffets, int num, int frameActuelle);
    void supprimerFramePressed(int page, int pageEffets, int num, int frameActuelle);
    void dupliquerFramePressed(int page, int pageEffets, int num, int frameActuelle, int dureeFrame);
    void copierFramePressed(int page, int pageEffets, int num, int frameActuelle, int dureeFrame);
    void collerFramePressed(int page, int pageEffets, int num, int frameActuelle);
    void sauverFrame(int page, int pageEffets, int num, int frame, int bouton, int couleur);
    void chargerFrame(int page, int pageEffets, int num, int frame);
    void update_supprimerPage(int page, int pageEffets, int num);
    void update_supprimerPageB(int page, int pageEffets, int num);
    void update_supprimerPageC(int page, int pageEffets, int num);
    void s_rafraichirSelection(int page, int pageEffets, int num);
private:
    QMdiArea *m_zoneCentrale;
    QString m_selectionActuelle;
    QSpinBox *m_frameActuelle;
    QSpinBox *m_dureeFrame;
    QPushButton *m_ajouterFrame;
    QPushButton *m_supprimerFrame;
    QPushButton *m_dupliquerFrame;
    QPushButton *m_copierFrame;
    QPushButton *m_collerFrame;
    QLabel *m_boutonActuel;
    QClickableLabel *m_bouton;
    QClickableLabel *m_boutonCouleur;
    std::vector< QClickableLabel* > m_listeBouton;
    int m_selectionCouleur;
    int m_page;
    int m_pageEffets;
    int m_num;
    bool m_clipboardRempli;
    FrameWorker *m_mainFWorker;
    FenetreSelection *m_ptrFenetreSelection;
    QProgressDialog *m_progress;
};

#endif // FENETREEDITION_H
