#include "containerframe.h"


ContainerFrame::ContainerFrame(const int page, const int pageEffets, const int num, int frame, int duree) : m_page(page), m_pageEffets(pageEffets), m_num(num), m_frame(frame), m_duree(duree)
{
    for (int i=0; i<80; i++)
    {
        m_listeBouton.push_back(0);
    }
}

ContainerFrame::~ContainerFrame()
{

}

void ContainerFrame::setDuree(int duree)
{
    m_duree = duree;
}

int ContainerFrame::duree() const
{
    return m_duree;
}

void ContainerFrame::setFrame(int frame)
{
    m_frame = frame;
}

int ContainerFrame::frame() const
{
    return m_frame;
}

int ContainerFrame::page() const
{
    return m_page;
}

int ContainerFrame::pageEffets() const
{
    return m_pageEffets;
}

int ContainerFrame::num() const
{
    return m_num;
}

void ContainerFrame::ajouterSupprBouton(int bouton, int couleur)
{
    if (couleur != 15)
    {
        m_listeBouton[bouton] = couleur+1;
    }
    else
    {
        m_listeBouton[bouton] = 0;
    }
}

std::vector< int > ContainerFrame::listeBouton()
{
    std::vector< int > listeBouton(80, 0);
    for (int i = 0; i < 80; i++) listeBouton[i] = m_listeBouton[i];
    return listeBouton;
}

void ContainerFrame::setListeBouton(std::vector< int > listeBouton)
{
    for(int i=0; i<80; i++)
    {
        m_listeBouton[i] = listeBouton[i];
    }
}

bool const ContainerFrame::existe(int page, int pageEffets, int num, int frame)
{
    if (page == m_page && pageEffets == m_pageEffets && num == m_num && frame == m_frame)
    {
        return true;
    }
    else if (page == m_page && pageEffets == m_pageEffets && num == m_num && frame == -1)
    {
        return true;
    }
    else if (page == m_page && pageEffets == m_pageEffets && num == -1 && frame == -1)
    {
        return true;
    }
    else if (page == m_page && pageEffets == -1 && num == m_num && frame == -1)
    {
        return true;
    }
    else if (page == m_page && pageEffets == -1 && num == -1 && frame == -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}
