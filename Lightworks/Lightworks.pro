#-------------------------------------------------
#
# Project created by QtCreator 2015-10-10T23:18:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lightworks
TEMPLATE = app


SOURCES += main.cpp\
        fenetreselection.cpp \
    qclickablelabel.cpp \
    fenetreedition.cpp \
    containerframe.cpp \
    qmdisubwindowmod.cpp \
    frameworker.cpp \
    myfilesystemmodel.cpp

HEADERS  += fenetreselection.h \
    qclickablelabel.h \
    fenetreedition.h \
    containerframe.h \
    qmdisubwindowmod.h \
    frameworker.h \
    myfilesystemmodel.h

FORMS    +=
